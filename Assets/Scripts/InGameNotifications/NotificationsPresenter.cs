using System;
using System.Collections.Generic;
using GameFlow.Client.UI.WindowPresenters;
using InGameNotifications.Data;
using InGameNotifications.View;
using InGameNotifications.View.NotificationView;
using UnityEngine;
using Zenject;

namespace InGameNotifications
{
    public class NotificationsPresenter : IInitializable, IDisposable
    {
        [Inject] private NotificationsService _notificationsService;
        [Inject] private NotificationAssets _notificationAssets;
        [Inject] private NotificationsView _notificationsView;
        
        private Dictionary<Type, GameObject> _map;

        public void Initialize()
        {
            InitMap();
            _notificationsService.OnNotificationAdded += OnNotificationAddedHandler;
        }
        
        public void Dispose()
        {
            _notificationsService.OnNotificationAdded -= OnNotificationAddedHandler;
        }

        /// <summary>
        /// Замапить все префабы по типу даты
        /// </summary>
        private void InitMap()
        {
            _map = new Dictionary<Type, GameObject>();
            foreach (GameObject prefab in _notificationAssets.Notifications)
            {
                INotificationView notificationView = prefab.GetComponent<INotificationView>();
                Type type = notificationView.GetType();
                foreach (Type it in type.GetInterfaces())
                {
                    if (it.IsGenericType && it.GetGenericTypeDefinition() == typeof(INotificationGenericView<>))
                    {
                        Type dataType = it.GetGenericArguments()[0];
                        _map.Add(dataType, prefab);
                        break;
                    }
                }
            }
        }
        
        private GameObject GetNotificationPrefab(Type dataType)
        {
            GameObject prefab = _map[dataType];
            return prefab;
        }

        private void OnNotificationAddedHandler(NotificationsService.INotificationInitializer<INotificationData> initializer)
        {
            GameObject prefab = GetNotificationPrefab(initializer.Data.GetType());
            GameObject instance = initializer.Spawn(prefab);
            _notificationsView.Add(instance);
        }
    }
}