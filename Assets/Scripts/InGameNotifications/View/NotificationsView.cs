﻿using DG.Tweening;
using InGameNotifications.View.DraggableView;
using InGameNotifications.View.NotificationView;
using UnityEngine;

namespace InGameNotifications.View
{
    public class NotificationsView : MonoBehaviour
    {
        [SerializeField] private RectTransform _animatedContent;
        [SerializeField] private RectTransform _notificationsRoot;
        [SerializeField] private float _offset;
        [SerializeField] private float _showDuration;
        
        private bool _isVisible;
        private bool _dragging;
        private Tween _hideTween;
        private AnimationMove _openOneNotificationAnimation;

        private UIDrag _uiDrag;
        private UIEndDragAnimation _endDragAnimation;

        private void OnEnable()
        {
            Vector2 closedPosition = _animatedContent.anchoredPosition.WithY(_animatedContent.rect.height);
            Vector2 openedPosition = closedPosition.WithAddedToY(-_offset);
            _animatedContent.anchoredPosition = closedPosition;
            
            Draggable.MakeDraggable(_animatedContent, null, null, closedPosition, openedPosition, Vector2.zero);
            
            _endDragAnimation = _animatedContent.GetComponent<UIEndDragAnimation>();
            _endDragAnimation.OnEndDrag += OnEndDragAnimationHandler;

            _uiDrag = _animatedContent.GetComponent<UIDrag>();
            _uiDrag.OnStartedDrag += OnStartedDragHandler;
            _uiDrag.OnEndedDrag += OnEndDragHandler;
            
            _openOneNotificationAnimation = new AnimationMove(null, _animatedContent, openedPosition);
        }

        private void OnDisable()
        {
            _endDragAnimation.OnEndDrag -= OnEndDragAnimationHandler;
            _uiDrag.OnStartedDrag -= OnStartedDragHandler;
            _uiDrag.OnEndedDrag -= OnEndDragHandler;
        }
        
        private void OnEndDragAnimationHandler(Vector2 pos)
        {
            if (Mathf.Approximately(pos.y, 1))
            {
                // Выключаем делей если закрыто
                StopDelayedHide();
            }
            else if (Mathf.Approximately(pos.y, 0))
            {
                // Выключаем делей если открыто
                StopDelayedHide();
            }
            else
            {
                // Если окно открыто не полностью, значит показывается только 1 нотификация, поэтому закрываем окно по делею
                HideNotificationWithDelay();
            }
        }

        private void OnStartedDragHandler()
        {
            _dragging = true;
            StopDelayedHide();
        }

        private void OnEndDragHandler()
        {
            _dragging = false;
        }

        private void HideNotificationWithDelay()
        {
            StopDelayedHide();
            
            _hideTween = DOVirtual.DelayedCall(_showDuration, () =>
            {
                _openOneNotificationAnimation.Hide();
                _isVisible = false;
                _hideTween = null;
            });
        }

        private void StopDelayedHide()
        {
            if (_hideTween != null)
            {
                _hideTween.Kill();
                _hideTween = null;
            }
        }

        public void Add(GameObject notification)
        {
            notification.transform.SetParent(_notificationsRoot);
            INotificationView notificationView = notification.GetComponent<INotificationView>();
            
            if (_dragging)
            {
                notificationView.Show();
            }
            else
            {
                if (_isVisible)
                {
                    notificationView.Show();
                    HideNotificationWithDelay();
                }
                else
                {
                    notificationView.ShowImmediate();
                    _isVisible = true;
                    _openOneNotificationAnimation.Show();
                    HideNotificationWithDelay();
                }
            }
        }
    }
}