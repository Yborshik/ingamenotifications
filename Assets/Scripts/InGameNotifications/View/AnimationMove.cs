using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace InGameNotifications.View
{
    public class AnimationMove
    {
        private readonly RectTransform _content;
        private Vector2 _startPosition;
        private readonly Vector2 _initialPosition;
        public Vector2 TargetPosition;
        private readonly Transform _root;

        private Tween _hiding;
        private Tween _showing;
        
        public AnimationMove(Transform root, RectTransform content, float offset)
        {
            _root = root;
            _content = content;
            _initialPosition = _content.anchoredPosition;
            
            TargetPosition = _initialPosition.WithAddedToY(-_content.rect.height + offset);
        }
        
        public AnimationMove(Transform root, RectTransform content, Vector2 targetPosition)
        {
            _root = root;
            _content = content;
            _initialPosition = _content.anchoredPosition;
            TargetPosition = targetPosition;
        }

        public void Show()
        {
            float distance = Vector2.Distance(_content.anchoredPosition, TargetPosition);
            var duration = distance / 4500;
            duration = 0.1f;

            _root?.gameObject.SetActive(true);
            
            TweenerCore<Vector2, Vector2, VectorOptions> tween = _content.DOAnchorPos(TargetPosition, duration);
            _showing = tween;
            
            if (_hiding != null)
                _hiding.Kill();

            _showing.OnComplete(() =>
            {
                _showing = null;
            });
        }
        
        public void Hide()
        {
            float distance = Vector2.Distance(_content.anchoredPosition, _initialPosition);
            var duration = distance / 4500;
            duration = 0.1f;
            
            _hiding = _content.DOAnchorPos(_initialPosition, duration)
                .OnComplete(() =>
                {
                    _hiding = null;
                    _root?.gameObject.SetActive(false);
                });
        }
    }
}