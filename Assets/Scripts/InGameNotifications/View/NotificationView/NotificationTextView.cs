﻿using InGameNotifications.Data;
using TMPro;
using UnityEngine;

namespace InGameNotifications.View.NotificationView
{
    public class NotificationTextView : NotificationView, INotificationGenericView<NotificationTextData>
    {
        [SerializeField] private TMP_Text _headerText;
        [SerializeField] private TMP_Text _bodyText;
        
        public void Init(NotificationTextData data)
        {
            _headerText.text = data.HeaderText;
            _bodyText.text = data.BodyText;
        }
    }
}