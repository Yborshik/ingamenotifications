﻿using UnityEngine;

namespace InGameNotifications.View.NotificationView
{
    public class NotificationView : MonoBehaviour, INotificationView
    {
        [SerializeField] private Animator _animator;
        private static readonly int ShowParameter = Animator.StringToHash("Show");
        private static readonly int ImmediateParameter = Animator.StringToHash("Immediate");
        
        public void Show()
        {
            _animator.SetBool(ShowParameter, true);
        }

        public void ShowImmediate()
        {
            _animator.SetBool(ShowParameter, true);
            _animator.SetTrigger(ImmediateParameter);
        }

        private void OnValidate()
        {
            _animator = GetComponent<Animator>();
        }
    }
}