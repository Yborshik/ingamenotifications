﻿using InGameNotifications.Data;

namespace InGameNotifications.View.NotificationView
{
    public interface INotificationGenericView<in T> : INotificationView where T : INotificationData
    {
        public void Init(T data);
    }
}