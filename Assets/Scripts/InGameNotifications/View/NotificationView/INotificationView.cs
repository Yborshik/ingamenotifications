﻿namespace InGameNotifications.View.NotificationView
{
    public interface INotificationView
    {
        void Show();
        void ShowImmediate();
    }
}