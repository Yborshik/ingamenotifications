using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InGameNotifications.View.DraggableView
{
    [RequireComponent(typeof(RectTransform))]
    public class UIDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private Vector2[] _snapAnchoredPositions;

        private bool Enabled = true;

        public event Action OnStartedDrag;
        public event Action OnEndedDrag;
        
        public Vector2 AnchoredPositionMin => _snapAnchoredPositions[_snapAnchoredPositions.Length - 1];
        public Vector2 AnchoredPositionMax => _snapAnchoredPositions[0];
        public Vector2[] SnapPositions => _snapAnchoredPositions;

        public Vector2 NormalizedPosition
        {
            get
            {
                Vector2 delta = AnchoredPositionMax - AnchoredPositionMin; // _anchoredPositionMax - _anchoredPositionMin;
                Vector2 anchoredPosition = _moveRect.anchoredPosition;

                Vector2 normalized;

                normalized.x = delta.x == 0 ? 0 : (anchoredPosition.x - AnchoredPositionMin.x) / delta.x;
                normalized.y = delta.y == 0 ? 0 : (anchoredPosition.y - AnchoredPositionMin.y) / delta.y;
            
                return normalized;
            }
        }
    
        private RectTransform _parentRect;
        private RectTransform _moveRect;

        private Vector2 _startPosition;
        private Vector2 _contentStartPosition;
        private bool _isDragging;

        private void Awake()
        {
            _parentRect = transform.parent.GetComponent<RectTransform>();
            _moveRect = GetComponent<RectTransform>();
        }

        /// <summary>
        /// Set positions from max to min
        /// </summary>
        /// <param name="positions">Anchored position</param>
        public void SetRestrictions(params Vector2[] positions)
        {
            _snapAnchoredPositions = positions;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!Enabled)
                return;
            
            _startPosition = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_parentRect, eventData.position, eventData.pressEventCamera, out _startPosition);
            _contentStartPosition = _moveRect.anchoredPosition;
            _isDragging = true;
            
            OnStartedDrag?.Invoke();
        }
    
        public void OnEndDrag(PointerEventData eventData)
        {
            _isDragging = false;
            OnEndedDrag?.Invoke();
        }
    
        public void OnDrag(PointerEventData eventData)
        {
            if (!_isDragging)
                return;

            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(_parentRect, eventData.position,
                    eventData.pressEventCamera, out Vector2 localCursor))
            {
                return;
            }
        
            Vector2 pointerDelta = localCursor - _startPosition;
            Vector2 position = _contentStartPosition + pointerDelta;

            SetAnchoredPosition(position);
        }

        private void SetAnchoredPosition(Vector2 position)
        {
            ClampAnchoredPosition(ref position);
            _moveRect.anchoredPosition = position;
        }

        private void ClampAnchoredPosition(ref Vector2 position)
        {
            position.x = Mathf.Clamp(position.x, AnchoredPositionMin.x, AnchoredPositionMax.x);
            position.y = Mathf.Clamp(position.y, AnchoredPositionMin.y, AnchoredPositionMax.y);
        }
    }
}
