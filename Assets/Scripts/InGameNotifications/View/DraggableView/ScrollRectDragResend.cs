using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace InGameNotifications.View.DraggableView
{
    [RequireComponent(typeof(ScrollRect))]
    public class ScrollRectDragResend : MonoBehaviour, IDragHandler, IResendReceiverHandler
    {
        [SerializeField] private ScrollRect _scrollRect;
    
        private GameObject _resendDragHandler;
        private IResendHandler[] _resendHandlers;
        private IResendReceiverHandler[] _resendReceivers;
    
        void Start()
        {
            FindResendTarget();
        }

        private void OnValidate()
        {
            if (_scrollRect == null)
            {
                _scrollRect = GetComponent<ScrollRect>();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            bool horizontalMove = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y);
            bool canResend = false;
            Vector2 normalizedPosition = new Vector2(Mathf.Clamp01(_scrollRect.normalizedPosition.x),
                Mathf.Clamp01(_scrollRect.normalizedPosition.y));

            if (!horizontalMove)
            {
                float sign = Mathf.Sign(eventData.delta.y);
                if (Mathf.Approximately(normalizedPosition.y, 1) && sign < 0)
                {
                    canResend = true;
                }

                if (Mathf.Approximately(normalizedPosition.y, 0) && sign > 0)
                {
                    canResend = true;
                }
            }

            if (!canResend || _resendDragHandler == null)
            {
                return;
            }
        
            if (_resendReceivers == null)
            {
                Resend(eventData);
            }
            else
            {
                canResend = _resendReceivers.All(handler => handler.CanReceiveResend(eventData));

                if (canResend)
                {
                    Resend(eventData);
                }
            }
        }

        public bool CanReceiveResend(PointerEventData eventData)
        {
            bool canResend = false;
            bool horizontalMove = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y);
            float sign = Mathf.Sign(eventData.delta.y);
            Vector2 normalizedPosition = new Vector2(Mathf.Clamp01(_scrollRect.normalizedPosition.x),
                Mathf.Clamp01(_scrollRect.normalizedPosition.y));
        
            if (!horizontalMove)
            {
                if (sign > 0 && Mathf.Approximately(normalizedPosition.y, 1))
                {
                    canResend = true;
                }

                if (sign < 0 && Mathf.Approximately(normalizedPosition.y, 0))
                {
                    canResend = true;
                }
            }

            return canResend;
        }

        private void FindResendTarget()
        {
            Transform parent = transform.parent;

            while (parent != null)
            {
                var dragHandler = parent.GetComponent<IDragHandler>();
                if (dragHandler != null)
                {
                    _resendDragHandler = parent.gameObject;
                    break;
                }

                parent = parent.parent;
            }

            if (_resendDragHandler != null)
            {
                _resendHandlers = _resendDragHandler.GetComponents<IResendHandler>();
                _resendReceivers = _resendDragHandler.GetComponents<IResendReceiverHandler>();
            }
        }
    
        private void Resend(PointerEventData eventData)
        {
            eventData.pointerDrag = _resendDragHandler;
            ExecuteEvents.Execute(_resendDragHandler, eventData, ExecuteEvents.beginDragHandler);
        
            if (_resendHandlers != null)
            {
                foreach (IResendHandler resendHandler in _resendHandlers)
                {
                    resendHandler.HandleResend(gameObject);
                }
            }
        }
    }
}
