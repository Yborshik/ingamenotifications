﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InGameNotifications.View.DraggableView
{
    public class UIDragResendHandler : MonoBehaviour, IResendHandler, IDragHandler, IEndDragHandler, IResendReceiverHandler
    {
        [SerializeField] private UIDrag _uiDrag;
        
        private GameObject _resenderSource;

        public void Init(UIDrag drag)
        {
            _uiDrag = drag;
        }
        
        public void HandleResend(GameObject source)
        {
            _resenderSource = source;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_resenderSource == null)
            {
                return;
            }
            
            bool horizontalMove = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y);
            bool canResend = false;
            Vector2 normalizedPosition = _uiDrag.NormalizedPosition;
            normalizedPosition = new Vector2(Mathf.Clamp01(normalizedPosition.x),
                Mathf.Clamp01(normalizedPosition.y));

            if (!horizontalMove)
            {
                float sign = Mathf.Sign(eventData.delta.y);
                if (sign > 0 && Mathf.Approximately(normalizedPosition.y, 1))
                {
                    canResend = true;
                }

                if (sign < 0 && Mathf.Approximately(normalizedPosition.y, 0))
                {
                    canResend = true;
                }
            }

            if (!canResend)
            {
                return;
            }

            Resend(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _resenderSource = null;
        }

        public bool CanReceiveResend(PointerEventData eventData)
        {
            bool canResend = false;
            bool horizontalMove = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y);
            float sign = Mathf.Sign(eventData.delta.y);
            Vector2 normalizedPosition = _uiDrag.NormalizedPosition;
            if (!horizontalMove)
            {
                if (sign > 0 && !Mathf.Approximately(normalizedPosition.y, 1))
                {
                    canResend = true;
                }

                if (sign < 0 && !Mathf.Approximately(normalizedPosition.y, 0))
                {
                    canResend = true;
                }
            }

            return canResend;
        }
        
        private void Resend(PointerEventData eventData)
        {
            IResendReceiverHandler[] canResendHandlers = _resenderSource.GetComponents<IResendReceiverHandler>();

            if (canResendHandlers == null)
            {
                eventData.pointerDrag = _resenderSource;
                ExecuteEvents.Execute(_resenderSource, eventData, ExecuteEvents.beginDragHandler);
            }
            else
            {
                bool canResend = canResendHandlers.All(handler => handler.CanReceiveResend(eventData));

                if (canResend)
                {
                    eventData.pointerDrag = _resenderSource;
                    ExecuteEvents.Execute(_resenderSource, eventData, ExecuteEvents.beginDragHandler);
                    _resenderSource = null;
                }
            }
        }
    }
}