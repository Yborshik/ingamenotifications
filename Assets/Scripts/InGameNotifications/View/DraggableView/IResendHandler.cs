﻿using UnityEngine;

namespace InGameNotifications.View.DraggableView
{
    public interface IResendHandler
    {
        public void HandleResend(GameObject source);
    }
}