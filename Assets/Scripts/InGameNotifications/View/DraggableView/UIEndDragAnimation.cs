using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InGameNotifications.View.DraggableView
{
    public class UIEndDragAnimation : MonoBehaviour, IDragHandler, IEndDragHandler
    {
        [SerializeField] private UIDrag _uiDrag;
        [SerializeField] private float _distanceToReturn = 50;
        [SerializeField] private float _speed = 4500;

        private Vector2 _lastDelta;

        public void Init(UIDrag drag)
        {
            _uiDrag = drag;
        }
        
        public event OnEndDragDelegate OnEndDrag;
    

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            float[] distances = new float[_uiDrag.SnapPositions.Length];
            
            Vector2 anchoredPosition = (_uiDrag.transform as RectTransform).anchoredPosition;

            for (var i = 0; i < _uiDrag.SnapPositions.Length; i++)
            {
                var position = _uiDrag.SnapPositions[i];
                distances[i] = Vector2.Distance(anchoredPosition, position);
            }
            
            int minPointIndex = -1;
            int maxPointIndex = -1;

            for (int i = 1; i < _uiDrag.SnapPositions.Length; i++)
            {
                Vector2 maxPoint = _uiDrag.SnapPositions[i - 1];
                Vector2 minPoint = _uiDrag.SnapPositions[i];

                if (IsInRange(anchoredPosition.x, minPoint.x, maxPoint.x)
                    && IsInRange(anchoredPosition.y, minPoint.y, maxPoint.y))
                {
                    minPointIndex = i;
                    maxPointIndex = i - 1;
                    break;
                }
            }

            float distanceToMin = distances[minPointIndex];
            float distanceToMax = distances[maxPointIndex];

            if (distanceToMin < distanceToMax)
            {
                HandleMinPosition(distanceToMin, _uiDrag.SnapPositions[minPointIndex], _uiDrag.SnapPositions[maxPointIndex]);    
            }
            else
            {
                HandleMaxPosition(distanceToMax, _uiDrag.SnapPositions[minPointIndex], _uiDrag.SnapPositions[maxPointIndex]);
            }
            
            _lastDelta = Vector2.zero;
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (eventData.delta != Vector2.zero)
            {
                _lastDelta = eventData.delta;
            }
        }

        private void HandleMinPosition(in float distance, in Vector2 minPosition, in Vector2 maxPosition)
        {
            if (distance < _distanceToReturn)
            {
                MoveToPosition(minPosition);
            }
            else
            {
                MoveByDirection(minPosition, maxPosition);
            }
        }
    
        private void HandleMaxPosition(in float distance, in Vector2 minPosition, in Vector2 maxPosition)
        {
            if (distance < _distanceToReturn)
            {
                MoveToPosition(maxPosition);
            }
            else
            {
                MoveByDirection(minPosition, maxPosition);
            }
        }

        private void MoveByDirection(in Vector2 minPosition, in Vector2 maxPosition)
        {
            bool moveToTop = _lastDelta.y > 0;
            if (moveToTop)
            {
                MoveToPosition(maxPosition);
            }
            else
            {
                MoveToPosition(minPosition);
            }
        }

        private void MoveToPosition(Vector2 position)
        {
            var rt = _uiDrag.transform as RectTransform;
            float distance = Vector2.Distance(rt.anchoredPosition, position);
            rt.DOAnchorPos(position, distance / _speed).SetEase(Ease.OutBack).OnComplete(() => OnEndDrag?.Invoke(_uiDrag.NormalizedPosition));
        }
        
        private bool IsInRange(in float value, in float min, in float max)
        {
            return min <= value && value <= max;
        }

        public delegate void OnEndDragDelegate(Vector2 normalizedPosition);
    }
}
