using UnityEngine;
using UnityEngine.EventSystems;

namespace InGameNotifications.View.DraggableView
{
    public class ResendToTarget : MonoBehaviour, IDragHandler
    {
        [SerializeField] private GameObject _resendDragTarget;

        public void OnDrag(PointerEventData eventData)
        {
            Resend(eventData);
        }
        
        private void Resend(PointerEventData eventData)
        {
            eventData.pointerDrag = _resendDragTarget;
            ExecuteEvents.Execute(_resendDragTarget, eventData, ExecuteEvents.beginDragHandler);
        }
    }
}