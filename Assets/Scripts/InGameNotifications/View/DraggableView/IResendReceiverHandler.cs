﻿using UnityEngine.EventSystems;

namespace InGameNotifications.View.DraggableView
{
    public interface IResendReceiverHandler
    {
        bool CanReceiveResend(PointerEventData eventData);
    }
}