using System;
using UnityEngine;

namespace InGameNotifications.View.DraggableView
{
    public class Draggable
    {
        public static void MakeDraggable(RectTransform content, Action open, Action close, params Vector2[] positions)
        {
            var contentGameObject = content.gameObject;
            
            UIDrag drag = contentGameObject.AddComponent<UIDrag>();
            UIEndDragAnimation endDragAnimation = contentGameObject.AddComponent<UIEndDragAnimation>();
            UIDragResendHandler t = contentGameObject.AddComponent<UIDragResendHandler>();
            t.Init(drag);
            endDragAnimation.Init(drag);
            drag.SetRestrictions(positions);
            
            endDragAnimation.OnEndDrag += (pos) =>
            {
                if (Mathf.Approximately(pos.y, 1))
                    open?.Invoke();
                if (Mathf.Approximately(pos.y, 0))
                    close?.Invoke();
            };
        }
    }
}