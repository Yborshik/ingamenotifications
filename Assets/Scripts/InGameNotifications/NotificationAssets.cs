using System.Collections.Generic;
using UnityEngine;

namespace InGameNotifications
{
    [CreateAssetMenu(fileName = "NotificationAssets", menuName = "Scriptable Objects/NotificationAssets", order = 0)]
    public class NotificationAssets : ScriptableObject
    {
        [SerializeField] private List<GameObject> _notifications;

        public List<GameObject> Notifications => _notifications;
    }
}