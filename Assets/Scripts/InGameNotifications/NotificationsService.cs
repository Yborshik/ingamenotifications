using System;
using System.Collections.Generic;
using InGameNotifications.Data;
using InGameNotifications.View.NotificationView;
using UnityEngine;
using Zenject;

namespace InGameNotifications
{
    public class NotificationsService
    {
        [Inject] private DiContainer _diContainer;

        public Action<INotificationInitializer<INotificationData>> OnNotificationAdded;
        
        private List<INotificationInitializer<INotificationData>> _notifications = new List<INotificationInitializer<INotificationData>>();

        public List<INotificationInitializer<INotificationData>> Notifications => _notifications;

        /// <summary>
        /// Добавить нотификацию
        /// </summary>
        public void AddNotification<T>(T data) where T : INotificationData
        {
            INotificationInitializer<INotificationData> initializer = CreateInitializer(data);
            _notifications.Add(initializer);
            OnNotificationAdded?.Invoke(initializer);
        }

        /// <summary>
        /// Создает инициализатор для конкретного типа данных
        /// </summary>
        private INotificationInitializer<INotificationData> CreateInitializer<T>(T data) where T : INotificationData
        {
            object obj = Activator.CreateInstance(typeof(NotificationInitializer<T>), data);
            _diContainer.Inject(obj);
            //NotificationInitializer<T> initializer = (NotificationInitializer<T>)obj;
            return (INotificationInitializer<INotificationData>) obj;
        }

        /// <summary>
        /// Интерфейс нужен для того что бы можно было хранить данные и инициализировать их в дженериковую вьюху
        /// </summary>
        public interface INotificationInitializer<out T> where T : INotificationData
        {
            T Data { get; }

            GameObject Spawn(GameObject prefab);
        }
        
        public class NotificationInitializer<T> : INotificationInitializer<T> where T : INotificationData
        {
            [Inject] private DiContainer _diContainer;

            public T Data { get; }

            public NotificationInitializer(T data)
            {
                Data = data;
            }

            public GameObject Spawn(GameObject prefab)
            {
                GameObject view = _diContainer.InstantiatePrefab(prefab);
                INotificationView notificationView = view.GetComponent<INotificationView>();
                if (notificationView is INotificationGenericView<T> genericView)
                {
                    genericView.Init(Data);
                }

                return view;
            }
        }
    }
}