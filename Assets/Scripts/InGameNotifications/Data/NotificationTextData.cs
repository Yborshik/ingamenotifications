﻿namespace InGameNotifications.Data
{
    public class NotificationTextData : INotificationData
    {
        public string HeaderText;
        public string BodyText;
    }
}