﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using InGameNotifications.Data;
using InGameNotifications.View;
using InGameNotifications.View.DraggableView;
using InGameNotifications.View.NotificationView;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace GameFlow.Client.UI.WindowPresenters
{
    public class NotificationWindowPresenter
    {
        
    } 
    /*
    StateWithUI<NotificationsView>
    {
        [Inject] private States _states;
        [Inject] private GameAssets _gameAssets;
        [Inject] private DiContainer _diContainer;

        private const float ShowDuration = 3f;

        private AnimationMove _openOneNotificationAnimation;
        private bool _isVisible;
        private bool _dragging;
        private Dictionary<Type, GameObject> _map;
        private List<INotificationInitializer<INotificationData>> _notifications = new List<INotificationInitializer<INotificationData>>();
        private Tween _hideTween;

        public void Init()
        {
            InitMap();
            Initialize();
            Test();
        }

        private async void Test()
        {
            // await Task.Delay(2000);
            // ShowNotification(new NotificationAttackData());
            await Task.Delay(5000);
            ShowNotification(new NotificationTextData(){HeaderText = "Hello", BodyText = "World"}); 
            await Task.Delay(2000);
            ShowNotification(new NotificationTextData(){HeaderText = "Header", BodyText = "Body"});
            await Task.Delay(2000);
            ShowNotification(new NotificationAttackData());
        }

        protected override void DoInitialize()
        {
            base.DoInitialize();
            
            Vector2 closedPosition = View.AnimatedContent.anchoredPosition.WithY(View.AnimatedContent.rect.height);
            Vector2 openedPosition = closedPosition.WithAddedToY(-View.Offset);
            var content = View.AnimatedContent;
            content.anchoredPosition = closedPosition;
            //Поменяны местами потому что нужно указывать позиции от верха до низа, это окно считается закрытым если контент в самом верху
            //(возможно нужно отрефакторить код если критично)
            Draggable.MakeDraggable(content, Close, ()=> { _states.FlushAndPush(this); }, closedPosition, openedPosition, Vector2.zero);
            
            UIEndDragAnimation endDragAnimation = content.GetComponent<UIEndDragAnimation>();
            endDragAnimation.OnEndDrag += (pos) =>
            {
                if (Mathf.Approximately(pos.y, 1))
                {
                    // Выключаем делей если закрыто
                    StopDelayedHide();
                }
                else if (Mathf.Approximately(pos.y, 0))
                {
                    // Выключаем делей если открыто
                    StopDelayedHide();
                }
                else
                {
                    // Если окно открыто не полностью, значит показывается только 1 нотификация, поэтому закрываем окно по делею
                    HideNotificationWithDelay();
                }
            };

            UIDrag uiDrag = content.GetComponent<UIDrag>();
            uiDrag.StartDrag += () =>
            {
                _dragging = true;
                StopDelayedHide();
            };

            uiDrag.EndDrag += () =>
            {
                _dragging = false;
            };
            
            View.ResendToTarget.Init(content.gameObject);
            Animation = new AnimationMove(null, content, Vector2.zero);
            _openOneNotificationAnimation = new AnimationMove(null, content, openedPosition);
        }
        
        public void ShowNotification<T>(T data) where T : INotificationData
        {
            GameObject instance = SpawnNotification(data, View.NotificationsRoot);
            AddNotification(data);
            INotificationInitializer<INotificationData> initializer = _notifications[^1];
            initializer.Init(instance);
            INotificationView notificationView = instance.GetComponent<INotificationView>();
            
            if (_dragging)
            {
                notificationView.Show();
            }
            else
            {
                if (_isVisible)
                {
                    notificationView.Show();
                    HideNotificationWithDelay();
                }
                else
                {
                    notificationView.ShowImmediate();
                    _isVisible = true;
                    _openOneNotificationAnimation.Show();
                    HideNotificationWithDelay();
                }
            }
        }

        /// <summary>
        /// Добавить нотификацию без спавна
        /// </summary>
        public void AddNotification<T>(T data) where T : INotificationData
        {
            INotificationInitializer<INotificationData> initializer = CreateInitializer(data);
            _notifications.Add(initializer);
        }

        /// <summary>
        /// После того как добавиви все старые нотификации нужно вызвать что бы их заспавнить
        /// </summary>
        public void Fill()
        {
            foreach (INotificationInitializer<INotificationData> initializer in _notifications)
            {
                GameObject notificationView = SpawnNotification(initializer.Data, View.NotificationsRoot);
                initializer.Init(notificationView);
            }
        }

        private void HideNotificationWithDelay()
        {
            StopDelayedHide();
            
            _hideTween = DOVirtual.DelayedCall(ShowDuration, () =>
            {
                _openOneNotificationAnimation.Hide();
                _isVisible = false;
                _hideTween = null;
            });
        }

        private void StopDelayedHide()
        {
            if (_hideTween != null)
            {
                _hideTween.Kill();
                _hideTween = null;
            }
        }

        /// <summary>
        /// Создает инициализатор для конкретного типа данных
        /// </summary>
        private INotificationInitializer<INotificationData> CreateInitializer<T>(T data) where T : INotificationData
        {
            object obj = Activator.CreateInstance(typeof(NotificationInitializer<T>), data);
            NotificationInitializer<T> initializer = (NotificationInitializer<T>)obj;
            return (INotificationInitializer<INotificationData>) initializer;
        }

        private GameObject SpawnNotification<T>(T data, RectTransform root) where T : INotificationData
        {
            Type dataType = data.GetType();
            GameObject prefab = _map[dataType];
            GameObject instance = Object.Instantiate(prefab, root);
            INotificationView notificationView = instance.GetComponent<INotificationView>();
            // Для производительности инджектим только вьюху
            // Так же можно инитить презентер во вьюху и через него использовать сервисные методы
            _diContainer.Inject(notificationView);
            return instance;
        }

        /// <summary>
        /// Замапить все префабы по типу даты
        /// </summary>
        private void InitMap()
        {
            _map = new Dictionary<Type, GameObject>();
            foreach (GameObject prefab in _gameAssets.Notifications)
            {
                INotificationView notificationView = prefab.GetComponent<INotificationView>();
                Type type = notificationView.GetType();
                foreach (Type it in type.GetInterfaces())
                {
                    if (it.IsGenericType && it.GetGenericTypeDefinition() == typeof(INotificationGenericView<>))
                    {
                        Type dataType = it.GetGenericArguments()[0];
                        _map.Add(dataType, prefab);
                        break;
                    }
                }
            }
        }
        
        /// <summary>
        /// Интерфейс нужен для того что бы можно было хранить данные и инициализировать их в дженериковую вьюху
        /// </summary>
        public interface INotificationInitializer<out T> where T : INotificationData
        {
            T Data { get; }

            void Init(GameObject prefab);
        }
        
        public class NotificationInitializer<T> : INotificationInitializer<T> where T : INotificationData
        {
            public T Data { get; }

            public NotificationInitializer(T data)
            {
                Data = data;
            }
            
            public void Init(GameObject view)
            {
                INotificationView notificationView = view.GetComponent<INotificationView>();
                if (notificationView is INotificationGenericView<T> genericView)
                {
                    genericView.Init(Data);
                }
            }
        }
    }
    */
}