using InGameNotifications;
using InGameNotifications.View;
using UnityEngine;
using Zenject;

public class Installer : MonoInstaller
{
    [SerializeField] private NotificationAssets _notificationAssets;
    [SerializeField] private NotificationsView _notificationsView;
    
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<NotificationsService>().AsSingle();
        Container.BindInterfacesAndSelfTo<NotificationsPresenter>().AsSingle();
        Container.BindInterfacesAndSelfTo<NotificationAssets>().FromInstance(_notificationAssets).AsSingle();
        Container.BindInterfacesAndSelfTo<NotificationsView>().FromInstance(_notificationsView).AsSingle();
    }
}
