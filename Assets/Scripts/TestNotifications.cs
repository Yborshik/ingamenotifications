using System.Threading.Tasks;
using InGameNotifications;
using InGameNotifications.Data;
using UnityEngine;
using Zenject;

public class TestNotifications : MonoBehaviour
{
    [Inject] private NotificationsService _notificationsService;

    private void OnEnable()
    {
        Test();
    }
    
    private async void Test()
    {
        await Task.Delay(1000);
        _notificationsService.AddNotification(new NotificationTextData(){HeaderText = "Hello", BodyText = "World"}); 
        await Task.Delay(2000);
        _notificationsService.AddNotification(new NotificationTextData(){HeaderText = "Header", BodyText = "Body"});
        await Task.Delay(2000);
        _notificationsService.AddNotification(new NotificationAttackData());
    }
}